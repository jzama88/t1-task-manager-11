package com.t1.alieva.tm.api;

import com.t1.alieva.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTask();

    void showTasks();

    void showProject(Task task);

    void showProjectById();

    void showProjectByIndex();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectById();

    void removeProjectByIndex();
}
