package com.t1.alieva.tm.api;

import com.t1.alieva.tm.model.Project;

public interface IProjectController {

    void createProject();

    void clearProject();

    void showProjects();

    void showProject(Project project);

    void showProjectById();

    void showProjectByIndex();

    void updateProjectByIndex();

    void updateProjectById();

    void removeProjectById();

    void removeProjectByIndex();
}
