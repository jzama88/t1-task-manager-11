package com.t1.alieva.tm.constant;

public final class TerminalConst {

    public final static String VERSION = "version";

    public final static String HELP = "help";

    public final static String ABOUT = "about";

    public final static String EXIT = "exit";

    public final static String INFO = "info ";

    public final static String ARGUMENTS = "arguments";

    public final static String COMMANDS = "commands";

    public final static String PROJECT_CLEAR = "project-clear";

    public final static String PROJECT_LIST = "project-list";

    public final static String PROJECT_CREATE = "project-create";

    public final static String TASK_CLEAR = "task-clear";

    public final static String TASK_LIST = "task-list";

    public final static String TASK_CREATE = "task-create";

    public final static String PROJECT_SHOW_BY_ID = "p-show-by-id";

    public final static String PROJECT_SHOW_BY_INDEX = "p-show-by-index";

    public final static String PROJECT_UPDATE_BY_ID = "p-update-by-id";

    public final static String PROJECT_UPDATE_BY_INDEX = "p-update-by-index";

    public final static String PROJECT_REMOVE_BY_INDEX = "p-remove-by-index";

    public final static String PROJECT_REMOVE_BY_ID = "p-remove-by-id";

    public final static String TASK_SHOW_BY_ID = "t-show-by-id";

    public final static String TASK_SHOW_BY_INDEX = "t-show-by-index";

    public final static String TASK_UPDATE_BY_ID = "t-update-by-id";

    public final static String TASK_UPDATE_BY_INDEX = "t-update-by-index";

    public final static String TASK_REMOVE_BY_INDEX = "t-remove-by-index";

    public final static String TASK_REMOVE_BY_ID = "t-remove-by-id";


}
