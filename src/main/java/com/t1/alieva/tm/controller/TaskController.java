package com.t1.alieva.tm.controller;


import com.t1.alieva.tm.api.ITaskController;
import com.t1.alieva.tm.api.ITaskService;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;
import com.t1.alieva.tm.service.TaskService;
import com.t1.alieva.tm.util.TerminalUtil;

import java.util.List;

public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }
    @Override
    public void createTask(){
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER TASK NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        if(task == null) System.out.println("[ERROR]");
        else System.out.println("[OK]"); }

    @Override
    public void clearTask(){
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void showTasks(){
        System.out.println("[TASK LIST]");
        final List<Task> tasks = taskService.findAll();
        int index = 1;
        for(final Task task:tasks){
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println("[OK]");
}

    @Override
    public void showProject(final Task task){
        if (task == null) return;
        System.out.println("ID: " +task.getId());
        System.out.println("NAME: " +task.getName());
        System.out.println("DESCRIPTION: " +task.getDescription());
    }

    @Override
    public void showProjectById(){
        System.out.println(("[SHOW TASK BY ID]"));
        System.out.println(("[ENTER ID]"));
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        showProject(task);
        System.out.println("[OK]");
    }

    @Override
    public void showProjectByIndex(){
        System.out.println(("[SHOW TASK BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.findOneByIndex(index);
        if (task == null){
            System.out.println("[FAIL]");
            return;
        }
        showProject(task);
        System.out.println("[OK]");
    }

    @Override
    public void updateProjectByIndex(){
        System.out.println(("[SHOW TASK BY INDEX]"));
        System.out.println(("[ENTER INDEX]"));
        final Integer index = TerminalUtil.nextNumber() -1;
        System.out.println(("[ENTER NAME]"));
        final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }


    @Override
    public void updateProjectById(){
        System.out.println(("[UPDATE TASK BY ID]"));
        System.out.println(("[ENTER ID]"));
        final String id = TerminalUtil.nextLine();
        System.out.println(("[ENTER NAME]"));
        final String name = TerminalUtil.nextLine();
        System.out.println(("[ENTER DESCRIPTION]"));
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }


    @Override
    public void removeProjectById(){
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.removeById(id);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

    @Override
    public void removeProjectByIndex(){
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() -1;
        final Task task = taskService.removeByIndex(index);
        if (task == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }
}
